<?php

namespace Drupal\commerce_mxmerchant\Derivative;

use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\PaymentGatewayStorageInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ClientPaymentGateway extends DeriverBase implements ContainerDeriverInterface {

  protected $paymentGatewayStorage;

  public function __construct(PaymentGatewayStorageInterface $payment_gateway_storage){
    $this->paymentGatewayStorage = $payment_gateway_storage;
  }

  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')->getStorage('commerce_payment_gateway')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $gateways = $this->paymentGatewayStorage->loadByProperties([
      'plugin' => 'mxmerchant_payment_gateway'
    ]);

    $this->derivatives = [];
    foreach ($gateways as $gateway) {
      $this->derivatives[$gateway->id()] = $base_plugin_definition;
      $this->derivatives[$gateway->id()]['admin_label'] = $gateway->label();
      $this->derivatives[$gateway->id()]['payment_gateway_id'] = $gateway->id();
      $this->derivatives[$gateway->id()]['config_dependencies']['content'] = [
        $gateway->getConfigDependencyName(),
      ];
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }
}
