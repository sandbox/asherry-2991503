<?php

namespace Drupal\commerce_mxmerchant;

use Drupal\apitools\ClientManager as ApiClientManager;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\user\UserInterface;

class ClientManager extends ApiClientManager {

  /**
   * Load the client plugin by entity hardcoded field.
   *
   * @param EntityInterface $entity
   *   The entity with a remote ID saved.
   *
   * @return bool|mixed
   */
  public function loadByEntity(EntityInterface $entity) {
    // TODO: Refactor this to a subfunction to handle checking if the provider exists.
    if ($entity instanceof PaymentInterface && $entity->get('payment_gateway')->target_id) {
      $provider = $entity->get('payment_gateway')->target_id;
    }
    if ($entity instanceof UserInterface && $entity->get('commerce_remote_id')->provider) {
      $provider = $entity->get('commerce_remote_id')->provider;
    }
    return !empty($provider) ? $this->load("commerce_mxmerchant:$provider") : NULL;
  }
}
