<?php

namespace Drupal\commerce_mxmerchant\PluginForm;

use Drupal\commerce\InlineFormManager;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as CommercePaymentMethodAddForm;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\user\UserInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PaymentMethodAddForm extends CommercePaymentMethodAddForm {

  use DependencySerializationTrait;

  /**
   * The current route match service.
   *
   * @var CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * {@inheritdoc}
   */
  public function __construct(CurrentStoreInterface $current_store, EntityTypeManagerInterface $entity_type_manager, InlineFormManager $inline_form_manager, LoggerInterface $logger, CurrentRouteMatch $current_route_match) {
    parent::__construct($current_store, $entity_type_manager, $inline_form_manager, $logger);
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_store.current_store'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_inline_form'),
      $container->get('logger.channel.commerce_payment'),
      $container->get('current_route_match')
    );
  }

  private function getDefaultUserPaymentGateway(UserInterface $user) {
    return $provider = $user->get('commerce_remote_id')->provider;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildCreditCardForm(array $element, FormStateInterface $form_state) {
    $element = parent::buildCreditCardForm($element, $form_state);
    // TODO: Submit to plugin does not capture current user, figure out how this form
    // should be capturing current user.
    if (!$user = $this->currentRouteMatch->getParameter('user')) {
      return $element;
    }

    $element['payment_gateway'] = [
      '#title' => $this->t('Payment gateway'),
      '#type' => 'select',
      '#options' => $this->getGatewayOptions($user),
      '#default_value' => $this->getDefaultUserPaymentGateway($user),
    ];

    return $element;
  }

  private function getGatewayOptions(UserInterface $user) {
    $storage = \Drupal::service('entity_type.manager')
      ->getStorage('commerce_payment_gateway');
    $options = [];
    foreach ($user->get('commerce_remote_id') as $item) {
      if (!$gateway = $storage->load($item->provider)) {
        continue;
      }
      $options[$item->provider] = $gateway->label();
    }
    return $options;
  }
}
