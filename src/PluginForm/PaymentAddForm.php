<?php


namespace Drupal\commerce_mxmerchant\PluginForm;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\OnsitePaymentAddForm;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class PaymentAddForm extends OnsitePaymentAddForm {

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    try {
      // Override to catch the PaymentGatewayException.
      parent::submitConfigurationForm($form, $form_state);
    }
    catch (PaymentGatewayException $e) {
      // TODO: Figure out why $form_state->setError causes a php serialization error.
      \Drupal::messenger()->addError($e->getMessage());
    }
    catch (RequestException $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
  }
}
