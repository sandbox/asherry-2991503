<?php

namespace Drupal\commerce_mxmerchant;

use Drupal\apitools\ModelControllerDefault as ApiToolsModelControllerDefault;
use Drupal\Component\Serialization\Json;

class ModelControllerDefault extends ApiToolsModelControllerDefault {

  /**
   * @param \GuzzleHttp\Psr7\Response $response
   * @return mixed|null
   */
  protected function processResponse($response) {
    if (!is_a($response, '\GuzzleHttp\Psr7\Response')) {
      return NULL;
    }
    $contents = $response->getBody()->getContents();
    $data = Json::decode($contents);
    // Seems to be a key of ['records'] when it's a root api call.
    return isset($data['records']) ? $data['records'] : $data;
  }
}

