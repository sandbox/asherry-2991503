<?php

namespace Drupal\commerce_mxmerchant\Plugin\Field\FieldFormatter;

use Drupal\commerce_mxmerchant\Plugin\Commerce\PaymentGateway\MXMerchantPaymentGateway;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\StringFormatter;

/**
 * Plugin implementation of the 'commerce_order_item_table' formatter.
 *
 * @FieldFormatter(
 *   id = "remote_response_code_message",
 *   label = @Translation("Remote Response Code Message"),
 *   field_types = {
 *     "string",
 *   },
 * )
 */
class RemoteResponseCodeMessageFormatter extends StringFormatter {

  /**
   * {@inheritdoc}
   */
  protected function viewValue(FieldItemInterface $item) {
    $message = $item->value ? MXMerchantPaymentGateway::getResponseMessageByCode($item->value) : '';
    return [
      '#type' => 'inline_template',
      '#template' => '{{ value|nl2br }}',
      '#context' => ['value' => $message],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getName() === 'remote_response_code';
  }
}
