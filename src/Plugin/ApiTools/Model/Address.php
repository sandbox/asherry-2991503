<?php

namespace Drupal\commerce_mxmerchant\Plugin\ApiTools\Model;

use Drupal\apitools\ModelBase;

/**
 * @ApiToolsModel(
 *   id = "mxmerchant_model_address",
 *   api = "mxmerchant",
 *   machine_name = "address",
 *   label = @Translation("Address"),
 *   controller = "\Drupal\commerce_mxmerchant\Plugin\ApiTools\Controller\Address",
 *   client_properties = {
 *     "addresses": {}
 *   }
 * )
 */
class Address extends ModelBase {
  
  protected $profile;

  public function isPrimary() {
    return $this->isDefault;
  }

  public function getCustomer() {
    return $this->getContext('mxmerchant_model_customer');
  }

  public function getProfile() {
    if (!isset($this->profile)) {
      if (!$this->id) {
        return FALSE;
      }
      $this->profile = $this->controller->getProfileByRemoteId($this->id);
    }
    return $this->profile;
  }
  public function setProfile($profile) {
    $this->profile = $profile;
    return $this;
  }

  protected function doInsert() {
    $customer = $this->getCustomer();
    $path = 'customer/' . $customer->id . '/address';
    $response = $this->controller->getClient()->post($path, [
      'json' => $this->values,
    ]);
    $location = $response->getHeader('Location');
    if (is_array($location)) {
      $location = reset($location);
    }
    $id = str_replace($this->controller->getClient()->url($path) . '/', '', $location);
    $this->id = $id;
    $profile = $this->profile;
    if (!$profile) {
      $profile = $customer->getProfile();
    }
    if ($profile) {
      $profile->set('commerce_remote_id', [
        'remote_id' => $id,
        'provider' => $this->controller->getClient()->getProviderName(),
      ]);
      $profile->save();
      $this->profile = $profile;
    }
  }

  protected function doUpdate() {
    $customer = $this->getCustomer();
    if (!$profile = $this->getProfile()) {
      return;
    }
    if ($profile_address = $profile->address->first()->getValue()) {
      foreach (self::addressFieldMapping('address') as $drupal_key => $mx_key) {
        $this->set($mx_key, $profile_address[$drupal_key]);
      }
    }
    $path = 'customer/' . $customer->id . '/address/' . $this->id;
    $response = $this->controller->getClient()->put($path, [
      'json' => $this->values,
    ]);
    if ($response->getStatusCode() != 200) {
      throw new \Exception($response->getReasonPhrase(), $response->getStatusCode());
    }
    return $response;
  }

  public function save() {
    if (!$customer = $this->getCustomer()) {
      return FALSE;
    }
    if (!$this->id) {
      return $this->doInsert();
    }
    return $this->doUpdate();
  }

  public function delete() {
    if (!$this->id || !$customer = $this->getCustomer()) {
      return FALSE;
    }
    return $this->controller->getClient()->delete('customer/' . $customer->id . '/address/' . $this->id);
  }

  /**
   * Map MXMerchant values to profile entity values.
   *
   * @param string $type
   * @return array
   */
  public static function addressFieldMapping($type = 'all') {
    $address = [
      'country_code' => 'country',
      'administrative_area' => 'state',
      'locality' => 'city',
      'postal_code' => 'zip',
      'address_line1' => 'address1',
      'address_line2' => 'address2',
    ];
    $customer = [
      'given_name' => 'firstName', 
      'family_name' => 'lastName', 
    ];
    $mappings = [];
    switch ($type) {
      case 'all':
        $mappings = array_merge($address, $customer);
        break;
      case 'address':
        $mappings = $address;
        break;
      case 'customer':
        $mappings = $customer;
        break;
    }
    return $mappings;
  }
}
