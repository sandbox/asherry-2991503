<?php

namespace Drupal\commerce_mxmerchant\Plugin\ApiTools\Model;

use Drupal\apitools\ModelBase;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\user\UserInterface;

/**
 * @ApiToolsModel(
 *   id = "mxmerchant_model_customer",
 *   api = "mxmerchant",
 *   machine_name = "customer",
 *   label = @Translation("Customer"),
 *   controller = "\Drupal\commerce_mxmerchant\Plugin\ApiTools\Controller\Customer",
 *   client_properties = {
 *     "customers": {
 *       "getAll": "customer",
 *       "get": "customer/{customer_id}",
 *     }
 *   },
 *   model_properties = {
 *     "mxmerchant_model_payment": {
 *       "getAll": "customer/{customer_id}/payment"
 *     },
 *     "mxmerchant_model_address": {
 *       "getAll": "customer/{customer_id}/address",
 *       "get": "customer/{customer_id}/address/{address_id}"
 *     },
 *     "mxmerchant_model_card_account": {
 *       "getAll": "customer/{customer_id}/cardaccount",
 *       "get": "customer/{customer_id}/cardaccount/{card_account_id}"
 *     }
 *   }
 * )
 */
class Customer extends ModelBase {

  /**
   * The user object to map to the remote customer.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The profile entity to use to map address values.
   *
   * Requires field "address".
   *
   * @var \Drupal\profile\Entity\ProfileInterface
   */
  protected $profile;

  /**
   * Get customer's name concatenated from MXMerchant.
   *
   * @return string
   */
  public function getName() {
    return isset($this->data['name']) ? $this->data['name'] : '';
  }

  /**
   * Get current user entity.
   *
   * @return UserInterface
   */
  public function getUser() {
    if (!isset($this->user) && $this->id) {
      $this->user = $this->controller->getUserByCustomerId($this->id);
    }
    return $this->user;
  }

  /**
   * Set current user entity.
   *
   * @param $user
   *
   * @return $this
   */
  public function setUser($user) {
    $this->user = $user;
    return $this;
  }

  /**
   * Get current profile entity.
   *
   * @return ProfileInterface
   */
  public function getProfile() {
    return $this->profile;
  }

  /**
   * Set current profile entity.
   *
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   The address enabled profile entity.
   *
   * @return $this
   */
  public function setProfile(ProfileInterface $profile) {
    $this->profile = $profile;
    return $this;
  }

  /**
   * Get the MXMerchant Gateway plugin id.
   *
   * @return string
   */
  public function getClientPluginId() {
    return $this->controller->getClient()->getPluginId();
  }

  /**
   * Check if there is no address, if so remove country.
   *
   * @param $values
   *   Array of address values to be spent to MXMerchant.
   * @return mixed
   */
  private function checkEmptyAddress($values) {
    $values_copy = $values;
    foreach (['firstName', 'lastName', 'country'] as $field) {
      if (!isset($values_copy[$field])) {
        continue;
      }
      unset($values_copy[$field]);
    }

    if (empty($values_copy) && isset($values['country'])) {
      unset($values['country']);
    }

    return $values;
  }

  private function setAddressValuesFromProfile(&$values, ProfileInterface $profile) {
    $address = $profile->get('address')->first()->getValue();
    foreach (Address::addressFieldMapping() as $drupal_key => $mx_key) {
      if (empty($address[$drupal_key])) {
        continue;
      }
      $values[$mx_key] = trim($address[$drupal_key]);
    }
    $values = array_map('trim', $values);
    // If there is no address, then remove the default country code if it
    // exists so that MxMerchant doesn't throw an error.
    $values = $this->checkEmptyAddress($values);
  }

  protected function doInsert() {
    // Find out if this is necessary.
    if (!$profile = $this->getProfile()) {
      throw new \Exception('No personal profile found.');
    }
    $values = [];
    $this->setAddressValuesFromProfile($values, $profile);

    $response = $this->controller->getClient()->post('customer', [
      'json' => $values,
    ]);
    $location = $response->getHeader('Location');
    $location = reset($location);
    $id = str_replace($this->controller->getClient()->url('customer') . '/', '', $location);
    $this->user->get('commerce_remote_id')->appendItem([
      'provider' => $this->controller->getClient()->getProviderName(),
      'remote_id' => $id,
    ]);
    $this->user->save();
    $this->id = $id;
    if ($address = $this->getPrimaryAddress()) {
      $profile = $this->getProfile();
      $profile->get('commerce_remote_id')->appendItem([
        'remote_id' => $address->id,
        'provider' => $this->controller->getClient()->getProviderName(),
      ]);
      $profile->save();
    }
    return $this;
  }

  protected function doUpdate() {
    $user = $this->getUser();
    if (!$profile = $this->getProfile()) {
      throw new \Exception('No personal profile found.');
    }

    $this->setAddressValuesFromProfile($values, $profile);
    $response = $this->controller->getClient()->put('customer/' . $this->id, [
      'json' => $values,
    ]);
    return $this;
  }

  public function save() {
    if (!$this->id) {
      return $this->doInsert();
    }
    else {
      return $this->doUpdate();
    }
  }

  public function delete() {
    if (!$this->id) {
      return FALSE;
    }
    return $this->controller->getClient()->delete('customer/' . $this->id);
  }

  public function getPrimaryAddress() {
    $default_address = array_filter($this->addresses->getAll(), function($address) {
      return $address->isPrimary();
    });
    return !empty($default_address) ? reset($default_address) : [];
  }

}
