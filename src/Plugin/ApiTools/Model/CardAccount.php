<?php

namespace Drupal\commerce_mxmerchant\Plugin\ApiTools\Model;

use Drupal\apitools\ModelBase;

/**
 * @ApiToolsModel(
 *   id = "mxmerchant_model_card_account",
 *   api = "mxmerchant",
 *   machine_name = "card_account",
 *   label = @Translation("Card Account"),
 *   client_properties = {
 *     "cardaccounts": {},
 *     "cardAccounts": {},
 *     "card_accounts": {}
 *   }
 * )
 */
class CardAccount extends ModelBase {
  
  public function save() {
    // TODO: Implement save method from gateway code.
  }

  public function getCustomer() {
    // TODO: Implement a CustomerContextAwareTrait and share with Address and Payment.
    return $this->getContext('mxmerchant_model_customer');
  }


  public function delete() {
    if (!$this->id || !$customer = $this->getCustomer()) {
      return FALSE;
    }
    // TODO: Remove the remote id from the entity if it exists.
    return $this->controller->getClient()->delete('customer/' . $customer->id . '/cardaccount/' . $this->id);
  }

}
