<?php

namespace Drupal\commerce_mxmerchant\Plugin\ApiTools\Model;

use Drupal\apitools\ModelBase;
use Drupal\commerce_payment\PaymentStorage;

/**
 * @ApiToolsModel(
 *   id = "mxmerchant_model_payment",
 *   api = "mxmerchant",
 *   machine_name = "payment",
 *   label = @Translation("Payment"),
 *   controller = "\Drupal\commerce_mxmerchant\Plugin\ApiTools\Controller\Payment",
 *   client_properties = {
 *     "payments": {
 *       "getAll": "payment",
 *       "get": "payment/{payment_id}"
 *     }
 *   }
 * )
 */
class Payment extends ModelBase {

  public function getStatus() {
    return $this->getValue('status');
  }

  public function getType() {
    return $this->getValue('type');
  }

  public function getAuthCode() {
    return $this->getValue('authCode');
  }

  public function getPaymentToken() {
    return $this->getValue('paymentToken');
  }

  public function getEntity() {
    //$gateway_id = $this->getController()->getClient()->getGatewayId();
    /** @var PaymentStorage $payment_storage */
    $payment_storage = \Drupal::service('entity_type.manager')->getStorage('commerce_payment');
    return $payment_storage->loadByRemoteId($this->id);
  }

  public function save() {}
}
