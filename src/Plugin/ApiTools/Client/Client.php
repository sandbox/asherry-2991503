<?php

namespace Drupal\commerce_mxmerchant\Plugin\ApiTools\Client;

use Drupal\apitools\ClientBase;
use Drupal\apitools\ClientManagerInterface;
use Drupal\apitools\ClientObjectManagerInterface;
use Drupal\apitools\ModelManagerInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\PaymentGatewayStorageInterface;
use Drupal\Component\Serialization\Json;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates a new SurveyMonkey Apply api client.
 *
 * @ApiToolsClient(
 *   id = "commerce_mxmerchant",
 *   admin_label = @Translation("Commerce MXMerchant Client"),
 *   api = "mxmerchant",
 *   deriver = "Drupal\commerce_mxmerchant\Derivative\ClientPaymentGateway",
 * )
 */
class Client extends ClientBase {

    /**
     * @var PaymentGatewayStorageInterface
     */
    protected $gatewayStorage;

    /**
     * @var String
     */
    protected $providerName;

    protected $gateway;

    private $consumerKey;

    private $consumerSecret;

    /**
     * {@inheritdoc}
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, ClientManagerInterface $client_manager, ClientObjectManagerInterface $object_manager, ModelManagerInterface $model_manager, PaymentGatewayStorageInterface $gateway_storage) {
      parent::__construct($configuration, $plugin_id, $plugin_definition, $client_manager, $object_manager);
      // Override this until we update this for the new API.
      $this->modelManager = $model_manager;
      $this->gatewayStorage = $gateway_storage;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
      return new static(
        $configuration,
        $plugin_id,
        $plugin_definition,
        $container->get('plugin.manager.apitools_client'),
        $container->get('plugin.manager.apitools_object'),
        $container->get('plugin.manager.apitools_model'),
        $container->get('entity_type.manager')->getStorage('commerce_payment_gateway')
      );
    }

    public function init(array $options = []) {
      $gateway_id = $this->pluginDefinition['payment_gateway_id'];
      $this->setGatewayId($gateway_id);
      return parent::init($options);
    }

    public function getGatewayId() {
      return $this->gateway ? $this->gateway->id() : NULL;
    }

    public function setGateway(PaymentGatewayInterface $gateway) {
      $this->gateway = $gateway;
      // get payment gateway from storage
      // find out if it's enabled
      // get test/production mode
      // get auth settings, key and what not
      // create a new mxmerchant client from that.
      $configuration = $gateway->getPluginConfiguration();

      $this->params = new \Drupal\apitools\Utility\ParameterBag([
        'oauth_callback' => '',
        'oauth_consumer_key' => '',
        'oauth_nonce' => sha1(microtime()),
        'oauth_signature_method' => 'HMAC-SHA1',
        'oauth_timestamp' => time(),
        'oauth_version' => '1.0'
      ]);

      $mode = $configuration['mode'];

      $base_uri = $mode == 'live' ? 'https://api.mxmerchant.com' : 'https://sandbox.api.mxmerchant.com';

      $this->options = new \Drupal\apitools\Utility\ParameterBag([
        'base_uri' => $base_uri,
        'base_path' => '/checkout/v3',
        'merchantId' => $configuration['merchant_id'],
      ]);

      $key_manager = \Drupal::service('key.repository');

      $consumer_key = $key_manager->getKey($configuration['consumer_key']);
      $key_values = $consumer_key->getKeyValues();
      if (!empty($key_values['consumer_key'])) {
        $consumer_key = $key_values['consumer_key'];
      }
      else {
        $consumer_key = $consumer_key->getKeyValue();
      }

      $consumer_secret = $key_manager->getKey($configuration['consumer_secret']);
      $key_values = $consumer_secret->getKeyValues();
      if (!empty($key_values['consumer_secret'])) {
        $consumer_secret = $key_values['consumer_secret'];
      }
      else {
        $consumer_secret = $consumer_secret->getKeyValue();
      }

      // Also check the getKeyValue to see if it actually has a proper value.
      if (!$consumer_key || !$consumer_secret) {
        return FALSE;
      }
      $this->consumerKey = $consumer_key;
      $this->params->set('oauth_consumer_key', $this->consumerKey);
      $this->consumerSecret = $consumer_secret;
      $this->tempStore = $this->manager->getTempStore($gateway->id());
      $this->httpClient = $this->manager->getClientFactory()->fromOptions($this->options->all());
      $this->providerName = $gateway->id();
      //$this->oauth($consumer_key, $consumer_secret);
      return $this;
    }

    public function setGatewayId($gateway_id) {
      if ($this->gateway && $this->gateway->id() == $gateway_id) {
        // Skip as this is already set and we don't need to re-authenticate.
        return $this;
      }
      if (!$gateway = $this->gatewayStorage->load($gateway_id)) {
        throw new \Exception('No gateway found.');
      }
      $this->setGateway($gateway);
      return $this;
    }

    /**
     * Get the current remote gateway.
     *
     * @return string
     */
    public function getProviderName() {
      return $this->providerName;
    }

    public function merchantId() {
      return $this->options->get('merchantId');
    }

    public function merchants() {
      $contents = $this->get('merchant')->getBody()->getContents();
      return Json::decode($contents);
    }

//    public function oauth($key, $secret) {
//      $this->consumerKey = trim($key);
//      $this->consumerSecret = trim($secret);
//      return $this;
//    }

    public function request($method, $path, $options = []) {
      $this->options->add($options);
      $path = $this->options->get('base_path') . '/' . $path;
      $this->signature($path, strtoupper($method));
      $parameters = [
        'headers' => [
          'Authorization' => 'OAuth ' . http_build_query($this->params->sort()->all(), '', ','),
          'Content-Type' => 'application/json',
        ],
      ];
      if ($this->options->get('json')) {
        $parameters['json'] = $this->options->get('json');
      }
      if ($this->options->get('query')) {
         $parameters['query'] = $this->options->get('query');
      }
      return $this->httpClient->{$method}($path, $parameters);
    }

    protected function loadTokens() {
      $token = $this->tempStore->get('oauth_token');
      $token_secret = $this->tempStore->get('oauth_token_secret');
      return !empty($token) && !empty($token_secret) ? $this->tempStore : FALSE;
    }

    protected function saveTokens() {
      $token = $this->params->get('oauth_token');
      $token_secret = $this->options->get('oauth_token_secret');

      $this->tempStore->set('oauth_token_secret', $token_secret);
      // Set to params to go straight to the header.
      $this->tempStore->set('oauth_token', $token);
      return $this;
    }
    protected function setTokens($token, $secret) {
      // Set to options to be used to generate the signature.
      $this->options->set('oauth_token_secret', $secret);
      // Set to params to go straight to the header.
      $this->params->set('oauth_token', $token);
      return $this;
    }

    public function refresh() {
      $this->tempStore->delete('oauth_token');
      $this->tempStore->delete('oauth_token_secret');
      return $this;
    }

    protected function auth() {
      if ($tokens = $this->loadTokens()) {
        return $this->setTokens($tokens->get('oauth_token'), $tokens->get('oauth_token_secret'));
      }

      foreach (['request', 'access'] as $type) {
        $response = $this->request('post', 'oauth/1a/' . $type . 'token')
          ->getBody()->getContents();
        $array = [];
        // Response is in the form of a url query string, so we need to parse.
        parse_str($response, $array);
        $this->setTokens($array['oauth_token'], $array['oauth_token_secret']);
      }
      return $this->saveTokens();
    }

    protected function signature($path, $method = 'POST') {
      $this->params->remove('oauth_signature');
      $url = $this->options->get('base_uri') . $path;
      // Sort oauth params set previously.
      $params = $this->params->sort()->all();
      if (!empty($this->options->get('query'))) {
        // Sort the query parameters also, otherwise signature will error.
        $query_params = $this->options->get('query');
        ksort($query_params);
        // Make sure their sorted for the ACTUAL request later on.
        $this->options->set('query', $query_params);
        // Merge query string params into regular params and sort again.
        $params =  array_merge($params, $query_params);
        // @see https://developer.twitter.com/en/docs/basics/authentication/oauth-1-0a/creating-a-signature
        ksort($params);
      }

      $oauth_params = http_build_query($params);
      $base_string = implode('&', [
        $method,
        $this->encode($url),
        $this->encode($oauth_params)
      ]);

      $key = $this->encode($this->consumerSecret) . '&';

      if ($secret = $this->options->get('oauth_token_secret')) {
        $key .= $secret;
      }
      $this->params->set('oauth_signature', base64_encode(hash_hmac('sha1', $base_string, $key, true)));
      return $this;
    }

    protected function encode($str) {
      return str_replace('+',' ',str_replace('%7E','~',rawurlencode($str)));
    }

}
