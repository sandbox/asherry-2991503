<?php

namespace Drupal\commerce_mxmerchant\Plugin\ApiTools\Controller;

use Drupal\commerce_mxmerchant\ModelControllerDefault;

class Address extends ModelControllerDefault {

  public static function addressFieldMapping($type = 'all') {
    return \Drupal\commerce_mxmerchant\Plugin\ApiTools\Model\Address::addressFieldMapping($type);
  }

  public function createFromProfile(\Drupal\profile\Entity\ProfileInterface $profile) {
    $model = $this->create([])->setProfile($profile);
    $address = $profile->address->first()->getValue();
    foreach (self::addressFieldMapping() as $drupal_key => $mx_key) {
      if (empty($address[$drupal_key])) {
        continue;
      }
      $model->set($mx_key, $address[$drupal_key]);
    }
    return $model;
  }

  public function getProfileByRemoteId($remote_id) {
    $profile_storage = \Drupal::service('entity_type.manager')->getStorage('profile');

    $profiles = $profile_storage->loadByProperties([
      'commerce_remote_id.remote_id' => $remote_id,
      'commerce_remote_id.provider' => $this->getClient()->getProviderName(),
    ]);
    if (!empty($profiles)) {
      $profile = reset($profiles);
      return $profile;
    }
    return FALSE;
  }

  /**
   * Loads an address with a customer context.
   */
  public function getByProfile(\Drupal\profile\Entity\ProfileInterface $profile) {
    $address = FALSE;
    if ($id = $profile->commerce_remote_id->remote_id) {
      if (!$customer = $this->getClient()->customers->getByProfile($profile)) {
        return $address;
      }
      try {
        $address = $customer->addresses->get($id);
      }
      catch (Exception $e) {
        watchdog_exception('commerce_mxmerchant', $e);
      }
    }
    return $address;
  }
}
