<?php

namespace Drupal\commerce_mxmerchant\Plugin\ApiTools\Controller;

use Drupal\commerce_mxmerchant\ModelControllerDefault;

class Payment extends ModelControllerDefault {

  public function export() {
    return $this->request('get', 'payment/export', [
      'json' => [
        'startDate' => new \DateTime('last year'),
        'limit' => 500,
      ],
    ]);
  }

  public function summary() {
    return $this->request('get', 'payment/summary');
  }

//  protected function doGetAll(array $options = []) {
//    if ($customer = $this->getContext('mxmerchant_model_customer')) {
//      $options['json']['customerId'] = $customer->id;
//      ksm($this->request('get', 'customer/' . $customer->id . '/payment', $options));
//    }
//  }
}
