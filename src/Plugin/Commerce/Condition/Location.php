<?php

namespace Drupal\commerce_mxmerchant\Plugin\Commerce\Condition;

use Drupal\commerce\EntityUuidMapperInterface;
use Drupal\commerce\Plugin\Commerce\Condition\ConditionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides the product type condition for orders.
 *
 * @CommerceCondition(
 *   id = "customer_location",
 *   label = @Translation("Customer location"),
 *   category = @Translation("Customer"),
 *   entity_type = "commerce_order",
 * )
 */
class Location extends ConditionBase implements ContainerFactoryPluginInterface {

  protected $entityFieldManager;
  protected $entityTypeManager;
  protected $entityUuidMapper;

  /**
   * Constructs a new OrderProductCategory object.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce\EntityUuidMapperInterface $entity_uuid_mapper
   *   The entity UUID mapper.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager, EntityUuidMapperInterface $entity_uuid_mapper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityUuidMapper = $entity_uuid_mapper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('commerce.entity_uuid_mapper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'nodes' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['nodes'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Locations'),
      '#default_value' => $this->getNodeIds(),
      '#options' => $this->getNodeOptions(),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    // Convert selected IDs into UUIDs, and store them.
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['nodes'] = $this->entityUuidMapper->mapFromIds('node', array_filter($values['nodes']));
    $this->configuration['nodes'] = array_values($this->configuration['nodes']);
  }

  public function getNodeId() {
    $ids = $this->getNodeIds();
    if (!empty($ids)) {
      $id = reset($ids);
      return $id;
    }
    return FALSE;
  }

  private function getNodeOptions() {
    $nodes = $this->entityTypeManager->getStorage('node')->loadByProperties(['type' => 'location']);
    return array_map(function($value) { return $value->label(); }, $nodes);
  }

  /**
   * Gets the configured term IDs.
   *
   * @return array
   *   The term IDs.
   */
  protected function getNodeIds() {
    return $this->entityUuidMapper->mapToIds('node', $this->configuration['nodes']);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity) {
    if (!$location = $entity->field_location->entity) {
      return TRUE;
    }
    if (empty($this->configuration['nodes'])) {
      return TRUE;
    }
    $nodes = $this->configuration['nodes'];
    return in_array($location->uuid(), $nodes);
  }

}
