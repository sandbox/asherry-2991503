<?php

namespace Drupal\commerce_mxmerchant\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;

/**
 * Provides the check payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "check",
 *   label = @Translation("Check"),
 * )
 */
class Check extends PaymentMethodTypeBase {
  public function buildLabel(PaymentMethodInterface $payment_method) {
    // TODO: Implement buildLabel() method.
  }
}
