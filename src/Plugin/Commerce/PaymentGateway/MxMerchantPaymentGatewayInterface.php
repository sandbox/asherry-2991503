<?php


namespace Drupal\commerce_mxmerchant\Plugin\Commerce\PaymentGateway;

use Drupal\apitools\ClientInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsUpdatingStoredPaymentMethodsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsVoidsInterface;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\user\UserInterface;

interface MxMerchantPaymentGatewayInterface extends OnsitePaymentGatewayInterface, SupportsAuthorizationsInterface, SupportsRefundsInterface, SupportsUpdatingStoredPaymentMethodsInterface {

  /**
   * Get an http client object for the current payment gateway.
   *
   * @return ClientInterface
   */
  public function getClient();

  /**
   * Create an MxMerchant customer and connect to a Drupal user.
   *
   * @param UserInterface $user
   *   The synced Drupal user object.
   * @param ProfileInterface $profile
   *   The Drupal Commerce address profile.
   * @return mixed
   */
  public function createCustomer(UserInterface $user, ProfileInterface $profile);
}
