<?php

namespace Drupal\commerce_mxmerchant\Plugin\Commerce\PaymentGateway;

use Drupal\apitools\ClientManagerInterface;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsVoidsInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\user\UserInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * TODO:
 * - fetch batches, or at least the batch for a particular payment
 * - looping through payments associated with a batch, and update the status
 *   if type for payment is "Return" it's the refund for another transaction.
 *   this means it will have "originalId".
 *
 * @CommercePaymentGateway(
 *   id = "mxmerchant_payment_gateway",
 *   label = @Translation("MX Merchant (onsite)"),
 *   display_label = @Translation("MX Merchant"),
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_mxmerchant\PluginForm\PaymentMethodAddForm",
 *     "edit-payment-method" = "Drupal\commerce_mxmerchant\PluginForm\PaymentMethodEditForm",
 *     "add-payment" = "Drupal\commerce_mxmerchant\PluginForm\PaymentAddForm",
 *   },
 *   payment_method_types = {"credit_card", "cash", "check"},
 *   default_payment_method_type = "credit_card",
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 *   },
 *   payment_type = "payment_mxmerchant",
 * )
 */
class MXMerchantPaymentGateway extends OnsitePaymentGatewayBase implements MxMerchantPaymentGatewayInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The ApiTools client manager service.
   *
   * @var \Drupal\apitools\ClientManagerInterface
   */
  protected $clientManager;

  /**
   * An instance of the Commerce MXMerchant client apitools plugin.
   *
   * @var \Drupal\commerce_mxmerchant\Plugin\ApiTools\Client\Client
   */
  protected $client;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, ClientManagerInterface $client_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->clientManager = $client_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('plugin.manager.apitools_client')
    );
  }

  public static function getResponseCodes() {
    /** @var ModuleExtensionList $extension_list */
    $extension_list = \Drupal::service('extension.list.module');
    $path = $extension_list->getPath('commerce_mxmerchant');
    $file = file_get_contents($path . '/authorization-codes.yml');
    $file_parsed = Yaml::decode($file);

    return array_reduce($file_parsed, function($carry, $item) {
      $carry[$item['responseCode']] = $item;
      return $carry;
    }, []);
  }

  public static function getResponseMessageByCode($code) {
    $codes = static::getResponseCodes();
    return $codes[$code]['message'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    // Implement your logic
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
        'merchant_id' => '',
        'consumer_key' => '',
        'consumer_secret' => '',
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['merchant_id'] = [
      '#title' => $this->t('Merchant ID'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['merchant_id'],
    ];

    $form['consumer_key'] = [
      '#title' => $this->t('Consumer Key'),
      '#type' => 'key_select',
      '#empty_option' => $this->t('-None-'),
      '#default_value' => $this->configuration['consumer_key'],
    ];

    $form['consumer_secret'] = [
      '#title' => $this->t('Consumer Secret'),
      '#type' => 'key_select',
      '#empty_option' => $this->t('-None-'),
      '#default_value' => $this->configuration['consumer_secret'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->cleanValues()->getValue($form['#parents']);
      foreach (['merchant_id', 'consumer_key', 'consumer_secret'] as $field) {
        $this->configuration[$field] = $values[$field];
      }
    }
  }
  /**
   * {@inheritdoc}
   */
  public function buildPaymentOperations(PaymentInterface $payment) {
    $operations = parent::buildPaymentOperations($payment);
    $payment_state = $payment->getState()->getId();
    $operations['void']['access'] = in_array($payment_state, ['authorization']);
    $operations['refund']['access'] = in_array($payment_state, ['settled', 'partially_refunded']);
    if ($this instanceof SupportsVoidsInterface) {
      $operations['void_sale'] = [
        'title' => $this->t('Void sale'),
        'page_title' => $this->t('Void payment sale'),
        'plugin_form' => 'void-payment',
        'access' => in_array($payment_state, ['completed']),
      ];
    }
    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function canVoidPayment(PaymentInterface $payment) {
    return $payment->getState()->getId() === 'authorization';
  }

  /**
   * {@inheritdoc}
   */
  public function getClient() {
    if (!isset($this->client)) {
      try {
        $this->client = $this->clientManager->load('commerce_mxmerchant:' . $this->parentEntity->id());
        $this->client->setGatewayId($this->parentEntity->id());
      }
      catch (PluginNotFoundException $e) {
        throw new PaymentGatewayException("Client plugin for {$this->parentEntity->id()} not found");
      }
    }
    return $this->client;
  }

  /**
   * {@inheritdoc}
   */
  public function createCustomer(UserInterface $user, ProfileInterface $profile = NULL) {
    $customer = $this->getClient()->customers->create([
      'user' => $user,
      'profile' => $profile,
    ]);
    return $customer;
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    // If $capture is FALSE then do authOnly = TRUE.
    $type = $payment->getPaymentMethod()->getType()->getPluginId();

    if ($payment->isNew()) {
      // Saves as "new" and will be labeled "In progress".
      $payment->save();
    }

    if ($type == 'check') {
      $this->createCheckPayment($payment, $capture);
    }
    if ($type == 'cash') {
      $this->createCashPayment($payment, $capture);
    }
    if ($type == 'credit_card') {
      $this->createCreditCardPayment($payment, $capture);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    // Check if payment is in the authorization status?
    // Run a regular payment process.
    $remote_payment = $this->getClient()->payments->get($payment->getRemoteId());
    if (!$remote_payment->get('authOnly')) {
      throw new PaymentGatewayException("MXMerchant payment {$payment->getRemoteId()} is not 'authOnly'");
    }
    $number = $amount ? $amount->getNumber() : $payment->getAmount()->getNumber();
    $auth_code = $remote_payment->getAuthCode();

    $data = [
      'merchantId' => $this->getClient()->merchantId(),
      'amount' => $number,
      'tenderType' => 'Card',
      'authOnly' => FALSE,
      'authCode' => $auth_code,
      'cardAccount' => [
        'token' => $this->getCardAccountTokenFromPayment($payment),
      ]
    ];
    $this->doPostPayment($data, $payment);
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    if (!$remote_id = $payment->getRemoteId()) {
      // TODO: This should probably throw an exception.
      return;
    }
    $this->doDeletePayment($payment, FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $refund_amount = $amount ?? $payment->getAmount();
    $payment->setRefundedAmount($refund_amount);
    $number = $amount->getNumber() * -1;
    if (!$payment->getBalance()->getNumber()) {
      // If no remaining balance, run delete.
      $this->doDeletePayment($payment, TRUE);
    }
    else {
      // Otherwise set partial refund.
      $data = [
        'merchantId' => $this->getClient()->merchantId(),
        'amount' => $number,
        'tenderType' => 'Card',
        'cardAccount' => [
          'token' => $this->getCardAccountTokenFromPayment($payment),
        ],
      ];
      $this->doPostPayment($data, $payment);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $values = [];
    $this->processPaymentMethod($values, $payment_method, $payment_details);
    $customer = $payment_method->customer;
    if (!$payment_method->isReusable()) {
      return;
    }
    try {
      $response = $this->getClient()->post("customer/{$customer->id}/cardaccount", [
        'json' => $values,
      ]);
      $location = $response->getHeader('Location');
      $location = reset($location);
      $id = str_replace($this->getClient()->url("customer/{$customer->id}/cardaccount/"), '', $location);
      $payment_method->setRemoteId($id);

      $expires = CreditCard::calculateExpirationTimestamp($payment_details['expiration']['month'], $payment_details['expiration']['year']);
      $payment_method->setExpiresTime($expires);
      $payment_method->save();
    }
    catch (ClientException $e) {
      throw new PaymentGatewayException($this->getExceptionDetailMessage($e));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentMethod(PaymentMethodInterface $payment_method) {
    // TODO: Implement updatePaymentMethod() method.
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    if (!$customer = $this->getClient()->customers->getByUid($payment_method->getOwnerId())) {
      return FALSE;
    }
    if ($remote_id = $payment_method->remote_id->getString()) {
      try {
        $this->getClient()->delete("customer/{$customer->id}/cardaccount/$remote_id", []);
        $payment_method->delete();
      }
      catch (RequestException $e) {
        throw new PaymentGatewayException($this->getExceptionDetailMessage($e));
      }
    }
  }

  /**
   * Sync remote payment status by remote ID.
   *
   * @param $id
   *   The remote MXMerchant ID.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function syncPaymentStatusById($id) {
    if (!$payment = Payment::load($id)) {
      return;
    }
    $this->syncPaymentStatus($payment);
  }

  /**
   * Sync remote status from MXMerchant to payment entity.
   *
   * @param PaymentInterface $payment
   *   The payment entity with the remote ID.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function syncPaymentStatus(PaymentInterface $payment) {
    if (!$remote_id = $payment->getRemoteId()) {
      throw new PaymentGatewayException('No remote payment ID found');
    }
    if (!$remote_payment = $this->getClient()->payments->get($remote_id)) {
      // setPaymentStatusFromResponse should be refactored a bit
      // to have a subfunction that handles the two statuses.
      throw new PaymentGatewayException('Remote payment not found');
    }

    $payment_status = $payment->getState()->value;
    $remote_status = $remote_payment->getStatus();
    if ($remote_status == 'Voided' && $payment_status == 'completed') {
      $payment->setState('sale_voided');
    }
    if ($remote_status == 'Settled' && $payment_status == 'completed') {
      // This is run from a script for now.
      $payment->setState('settled');
    }
    $payment->save();
  }

  private function setPaymentStatusFromDelete(PaymentInterface $payment, $force) {
    if ($force) {
      $payment->setState('refunded');
    }
    else {
      $payment->setState('sale_Voided');
    }
    $payment->setCompletedTime(NULL);
    $payment->save();
  }

  private function getExceptionDetailMessage(RequestException $e) {
    $response = $e->getResponse()->getBody()->getContents();
    $json = Json::decode($response);
    return !empty($json['details']) ? reset($json['details']) : $e->getMessage();

  }

  private function doDeletePayment(PaymentInterface $payment, $force) {
    $remote_id = $payment->getRemoteId();
    $params = [];
    if ($force) {
      $params['query']['force'] = 'true';
    }
    try {
      /** @var Response $response */
      $response = $this->getClient()->delete("payment/$remote_id", $params);
      if ($response->getStatusCode() === 204) {
        $this->setPaymentStatusFromDelete($payment, $force);
      }
    }
    catch (RequestException $e) {
      $code = $e->getCode();
      if ($code === 422) {
        // Already refunded.
        $this->setPaymentStatusFromDelete($payment, $force);
        throw new PaymentGatewayException('Error: Payment ' . $payment->id() . ' has already been refunded or voided');
      }
      throw new PaymentGatewayException($this->getExceptionDetailMessage($e));
    }
  }

  private function getCardAccountToken(PaymentMethodInterface $payment_method) {
    $user = $payment_method->get('uid')->entity;
    // TODO: Depends on User class override.
    $customer_id = $user->getRemoteId();
    if (!$card_account_id = $payment_method->getRemoteId()) {
      throw new PaymentGatewayException('Unable to fetch saved payment method information');
    }

    $request = $this->getClient()->get("customer/$customer_id/cardaccount/$card_account_id", []);
    $json = $request->getBody()->getContents();
    $card_account = Json::decode($json);
    return $card_account['token'];
  }

  /**
   * Ensure payment method has remote ID and fetch token.
   *
   * @param PaymentInterface $payment
   * @return mixed|null
   */
  private function getCardAccountTokenFromPayment(PaymentInterface $payment) {
    if (!$payment_method = $payment->getPaymentMethod()) {
      return NULL;
    }
    if (!$payment_method->getRemoteid() && $payment_method->payment_details) {
      $this->createPaymentMethod($payment_method, $payment_method->payment_details);
    }
    return $this->getCardAccountToken($payment_method);
  }

  /**
   * Create a new credit card payment.
   *
   * Remote statuses:
   *   Approved, Declined, Settled, Voided, ChargedBack, TicketOnly, AuthOnly, InProgress.
   */
  protected function createCreditCardPayment(PaymentInterface $payment, $capture) {
    /** @var PaymentMethodInterface $payment_method */
    $payment_method = $payment->getPaymentMethod();

    // create credit card payment
    // return payment
    // then queue for processing?
    // - in the front end we create the payment
    // - then we create another request for "authorizing payment"
    // - that will then be maybe a ...?
    $data = [
      'merchantId' => $this->getClient()->merchantId(),
      'amount' => $payment->getAmount()->getNumber(),
      'tenderType' => 'Card',
      // need something besides payment id.
      'replayId' => $payment->id(),
      'customer' => [
        'id' => $this->getPaymentCustomerId($payment),
      ],
      'cardAccount' => [],
    ];
    // Get existing payment method set already for this payment.
    if ($payment_method->isReusable()) {
      $data['cardAccount']['token'] = $this->getCardAccountTokenFromPayment($payment);
    }
    else {
      // I think $payment_method->payment_details is only used because I never
      // customized the core form for submitting a payment.
      $this->processPaymentMethod($data['cardAccount'], $payment_method, $payment_method->payment_details);

      if (!$this->arrayKeysExist($data['cardAccount'], ['cvv', 'expirymonth', 'expiryyear', 'addressid', 'avsStreet', 'avsZip'])) {
        throw new PaymentGatewayException('Unable to process payment method data');
      }
    }

    if (!$capture) {
      $data['authOnly'] = TRUE;
    }
    $this->doPostPayment($data, $payment);
  }

  private function arrayKeysExist(array $array, array $keys) {
    return count(array_intersect_key(array_flip($keys), $array)) === count($keys);
  }

  protected function createCashPayment(PaymentInterface $payment, $capture) {
    $customer_id = $this->getPaymentCustomerId($payment);
    $json = [
      'merchantId' => $this->getClient()->merchantId(),
      'amount' => $payment->getAmount()->getNumber(),
      'tenderType' => 'Cash',
      'customer' => [
        'id' => $customer_id,
      ],
    ];
    $this->doPostPayment($json, $payment);
  }

  protected function createCheckPayment(PaymentInterface $payment, $capture) {
    $customer_id = $this->getPaymentCustomerId($payment);
    $json = [
      'merchantId' => $this->getClient()->merchantId(),
      'amount' => $payment->getAmount()->getNumber(),
      'tenderType' => 'Check',
      'customer' => [
        'id' => $customer_id,
      ],
    ];
    $this->doPostPayment($json, $payment);
  }

  private function doPostPayment($data, PaymentInterface $payment = NULL) {
    if ($payment->isNew()) {
      $payment->save();
    }
    try {
      if (empty($data['invoice'])) {
        $data['invoice'] = $payment->getOrderId();
      }
      $response = $this->getClient()->post('payment', [
        'json' => $data,
        'query' => ['echo' => 'true'],
      ]);
      if ($payment) {
        $response_data = Json::decode($response->getBody()->getContents());
        $this->setPaymentRemoteIdFromResponseData($payment, $response_data);
        $this->setPaymentStatusFromResponseData($payment, $response_data);
        $payment->save();
      }
    }
    catch (RequestException $e) {
      throw new PaymentGatewayException($e->getMessage());
    }
  }

  private function getPaymentCustomerId(PaymentInterface $payment) {
    $user = $payment->getOrder()->getCustomer();
    foreach ($user->get('commerce_remote_id') as $item) {
      if ($item->provider === $this->parentEntity->id()) {
        return $item->remote_id;
      }
    }
    return NULL;
  }

  /**
   * @param PaymentInterface $payment
   *   The payment entity.
   * @param ResponseInterface $response
   *   Http response from the payment endpoint.
   */
  private function setPaymentStatusFromResponseData(PaymentInterface $payment, $response_data) {
    $status_code = $response_data['responseCode'] ?? NULL;
    $status = $response_data['status'] ?? NULL;
    if (!isset($status_code) || !isset($status)) {
      return;
    }
    $payment->setRemoteState($status);
    $payment->set('remote_response_code', $status_code);

    switch ($status_code) {
      case 5:
        // Do not honor
        $payment->setState('declined');
        $log_template = 'order_payment_failure';
        break;
      case 10:
        // Partial Approval
        // TODO: Find out if this relates to authorization.
        $payment->setState('authorization');
        $log_template = 'order_payment_success';
        break;
      case 51:
        // Decline - Insufficient funds
        $payment->setState('declined');
        $log_template = 'order_payment_failure';
        break;
      case 61:
        // Decline - Exceeds withdrawl
        $payment->setState('declined');
        $log_template = 'order_payment_failure';
        break;
      case 96:
        // System error
        // TODO: Do not allow re-try
        $payment->setState('declined');
        $log_template = 'order_payment_failure';
        break;
      default:
        if ($status == 'Declined' || $status == 'Decline') {
          $payment->setState('declined');
          $log_template = 'order_payment_failure';
        }
        else if ($status == 'Approved') {
          $payment->setState('completed');
          $log_template = 'order_payment_success';
        }
        else {
          $log_template = 'order_payment_failure';
        }
    }

    $this->entityTypeManager->getStorage('commerce_log')->generate($payment->getOrder(), $log_template, [
      'message' => static::getResponseMessageByCode($status_code),
    ])->save();
  }

  private function setPaymentRemoteIdFromResponseData(PaymentInterface $payment, array $response_data) {
    if (!empty($response_data['id'])) {
      $payment->setRemoteId($response_data['id']);
    }
  }

  /**
   * Populate data array and prepare for post request.
   *
   * @param $data
   *   Array of data to POST for a payment, or a payment method.
   * @param PaymentMethodInterface $payment_method
   *   Payment method entity.
   * @param array $payment_details
   *   Array of payment details sent from input.
   */
  private function processPaymentMethod(&$data, PaymentMethodInterface $payment_method, array $payment_details) {
    $billing_profile = $payment_method->get('billing_profile')->entity;
    // Skip if there is no user, because that means there is no customer.
    if (empty($billing_profile->uid->entity)) {
      throw new PaymentGatewayException('Unable to create payment method, no user found');
    }
    /**
     *  @example
     *
     *  $payment_details = [
     *    'type' => 'visa',
     *    'number' => '4111111111111111',
     *    'expiration' => [
     *      'month' => '07',
     *      'year' => '2019',
     *    ],
     *    'security_code' => '242',
     *  ];
     */
    $user = $billing_profile->uid->entity;
    // TODO: Make sure in the core commerce forms that billing profile is
    // populated, otherwise this will error out.
    if (!$customer = $payment_method->customer) {
      $customer = $this->getClient()->customers->getByUser($user);
    }
    if (empty($customer->id)) {
      throw new PaymentGatewayException('No customer found.');
    }
    // TODO: I'm not sure that creating an address is necessary
    // if it's not a reusable card.
    if (!$address_id = $billing_profile->commerce_remote_id->remote_id) {
      try {
        $address = $customer->addresses->createFromProfile($billing_profile);
        $address->save();
      }
      catch (ClientException $e) {
        throw new PaymentGatewayException($e->getMessage());
      }
      $address_id = $address->id;
    }
    else {
      $address = $customer->addresses->get($address_id);
    }
    // If they didn't provide a "name on card value", assume it's the billing address name.
    // TODO: This is mainly for the current form, this should be fixed.
    if (empty($payment_details['name'])) {
      $info = $billing_profile->address->first()->getValue();
      $payment_details['name'] = join(' ', [$info['given_name'], $info['additional_name'], $info['family_name']]);
    }
    $data = [
      'customer' => [
        'id' => $customer->id
      ],
      'number' => $payment_details['number'],
      'expirymonth' => $payment_details['expiration']['month'],
      'expiryyear' => $payment_details['expiration']['year'],
      'cvv' => $payment_details['security_code'],
      'name' => $payment_details['name'],
      'addressid' => $address_id,
      'avsStreet' => trim($address->get('address1')),
      'avsZip' => trim($address->get('zip')),
    ];
  }
}
