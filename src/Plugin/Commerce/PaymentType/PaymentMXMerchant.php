<?php

namespace Drupal\commerce_mxmerchant\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;

/**
 * Provides the MXMerchant payment type.
 *
 * @CommercePaymentType(
 *   id = "payment_mxmerchant",
 *   label = @Translation("MXMerchant"),
 *   workflow = "payment_mxmerchant",
 * )
 */
class PaymentMXMerchant extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    return [];
  }

}
